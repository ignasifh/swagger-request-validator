package com.atlassian.oai.validator.interaction;

import com.atlassian.oai.validator.model.ApiOperation;
import com.atlassian.oai.validator.model.ApiOperationMatch;
import com.atlassian.oai.validator.model.ApiPath;
import com.atlassian.oai.validator.model.ApiPathImpl;
import com.atlassian.oai.validator.model.NormalisedPath;
import com.atlassian.oai.validator.model.NormalisedPathImpl;
import com.atlassian.oai.validator.model.Request;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.Paths;
import io.swagger.v3.oas.models.servers.Server;
import org.slf4j.Logger;

<<<<<<< HEAD
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Comparator.comparingInt;
=======
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.net.URI;
import java.net.URISyntaxException;
>>>>>>> remotes/atlassian/swagger-request-validator/master
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

<<<<<<< HEAD
=======
import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static java.util.Comparator.comparingInt;
>>>>>>> remotes/atlassian/swagger-request-validator/master
import static java.util.Optional.ofNullable;
import java.util.function.BiPredicate;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
<<<<<<< HEAD
import java.util.stream.Stream;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
=======
import static org.slf4j.LoggerFactory.getLogger;
>>>>>>> remotes/atlassian/swagger-request-validator/master

/**
 * Component responsible for matching an incoming request path + method with an operation defined in the OAI spec.
 */
public class ApiOperationResolver {

    private static final Logger log = getLogger(ApiOperationResolver.class);

    private final String apiPrefix;

    private final List<ApiPath> apiPaths;
    private final Map<Integer, List<ApiPath>> apiPathsGroupedByNumberOfParts;
    private final Table<String, PathItem.HttpMethod, Operation> operations;

    /**
     * A utility for finding the best fitting API path.
     *
     * @param api              the OpenAPI definition
     * @param basePathOverride (Optional) override for the base path defined in the OpenAPI specification.
     */
<<<<<<< HEAD
    public ApiOperationResolver(@Nonnull final Swagger api, @Nullable final String basePathOverride) {
        apiPrefix = ofNullable(basePathOverride).orElse(api.getBasePath());
        final Map<String, Path> apiPathsByURI = ofNullable(api.getPaths()).orElse(emptyMap());
=======
    public ApiOperationResolver(final OpenAPI api, @Nullable final String basePathOverride) {

        apiPrefix = ofNullable(basePathOverride).orElse(getBasePathFrom(api.getServers()));
        final Paths apiPaths = ofNullable(api.getPaths()).orElse(new Paths());
>>>>>>> remotes/atlassian/swagger-request-validator/master

        apiPaths = apiPathsByURI.keySet().stream()
                .map(p -> new ApiPathImpl(p, apiPrefix))
                .collect(toList());
        // normalise all API paths and group them by their number of parts
        apiPathsGroupedByNumberOfParts = apiPaths
                .stream()
                .collect(groupingBy(NormalisedPath::numberOfParts));

        // create a operation mapping for the API path and HTTP method
        operations = HashBasedTable.create();
<<<<<<< HEAD
        apiPathsByURI.forEach((pathKey, apiPath) ->
                apiPath.getOperationMap().forEach((httpMethod, operation) ->
=======
        apiPaths.forEach((pathKey, apiPath) ->
                apiPath.readOperationsMap().forEach((httpMethod, operation) ->
>>>>>>> remotes/atlassian/swagger-request-validator/master
                        operations.put(pathKey, httpMethod, operation))
        );
    }

    /**
     * Tries to find the best fitting API path matching the given path and request method.
     *
     * @param path   the requests path to find in API definition
     * @param method the {@link Request.Method} for the request
     * @return a {@link ApiOperationMatch} containing the information if the path is defined, the operation
     * is allowed and having the necessary {@link ApiOperation} if applicable
     */
    @Nonnull
<<<<<<< HEAD
    public ApiOperationMatch findApiOperation(@Nonnull final String path, @Nonnull final Request.Method method) {
        final NormalisedPath requestPath = new NormalisedPathImpl(path, apiPrefix);
        //When using the standard path matching algorithm we can optimize by only considering paths with a matching number of parts
        final Stream<ApiPath> pathsWithMatchingNumberOfParts = apiPathsGroupedByNumberOfParts.getOrDefault(requestPath.numberOfParts(), emptyList()).stream();
        return findApiOperation(requestPath, method, ApiPath::matches, pathsWithMatchingNumberOfParts);
    }
=======
    public ApiOperationMatch findApiOperation(final String path, final Request.Method method) {
>>>>>>> remotes/atlassian/swagger-request-validator/master

    /**
     * Tries to find the best fitting API path matching the given path and request method, given a custom path
     * matching method.
     *
     * @param path   the requests path to find in API definition
     * @param method the {@link Request.Method} for the request
     * @param matcher a function that can compare an API path from a specification to a request path to see if they match, e.g. {@code ApiPath::matches}
     * @return a {@link ApiOperationMatch} containing the information if the path is defined, the operation
     * is allowed and having the necessary {@link ApiOperation} if applicable
     */
    @Nonnull
    public ApiOperationMatch findApiOperation(@Nonnull final String path, @Nonnull final Request.Method method,
            @Nonnull final BiPredicate<ApiPath, NormalisedPath> matcher) {
        final NormalisedPath requestPath = new NormalisedPathImpl(path, apiPrefix);
        return findApiOperation(requestPath, method, matcher, apiPaths.stream());
    }

    @Nonnull
    private ApiOperationMatch findApiOperation(@Nonnull final NormalisedPath requestPath, @Nonnull final Request.Method method,
            @Nonnull final BiPredicate<ApiPath, NormalisedPath> matcher, @Nonnull final Stream<ApiPath> paths) {
        // try to find possible matching paths regardless of HTTP method
        final List<ApiPath> matchingPaths = paths
                .filter(apiPath -> matcher.test(apiPath, requestPath))
                .collect(toList());

        if (matchingPaths.isEmpty()) {
            return ApiOperationMatch.MISSING_PATH;
        }

        // try to find the operation which fits the HTTP method,
        // choosing the most 'specific' path match from the candidates
        final PathItem.HttpMethod httpMethod = PathItem.HttpMethod.valueOf(method.name());
        final Optional<ApiPath> matchingPathAndOperation = matchingPaths.stream()
                .filter(apiPath -> operations.contains(apiPath.original(), httpMethod))
                .max(comparingInt(ApiOperationResolver::specificityScore));

        return matchingPathAndOperation
                .map(match ->
                        new ApiOperationMatch(new ApiOperation(match, requestPath, httpMethod, operations.get(match.original(), httpMethod))))
                .orElse(ApiOperationMatch.NOT_ALLOWED_OPERATION);
    }

    /**
     * Get the 'specificity' score of the provided API path. This is used when selecting an API operation to validate against -
     * where an incoming request matches multiple paths the 'most specific' one should win.
     * <p>
     * Note: This score is essentially meaningless across different paths - it should only be used to differentiate paths
     * that could be equivalent. For example, '{@code /{id}}' and '{@code /{id}.json}' could both match an incoming request on path
     * '{@code /foo.json}'; in that case we should match on '{@code /{id}.json}' as it is the most 'specific' match.
     *
     * @return a score >= 0 that indicates how 'specific' the path definition is. Higher numbers indicate more specific
     * definitions (e.g. fewer path variables).
     */
    private static int specificityScore(final ApiPath apiPath) {
        // Return the length of the path, with path vars counting as 1.
        return apiPath.normalised().replaceAll("\\{.+?}", "").length();
    }

    /**
     * Determine the 'base path' of the given API.
     * <p>
     * Returns the base path of the first server definition in the spec.
     *
     * @param servers The OpenAPI servers definition to get the base path from
     *
     * @return the base path of the first server definition found in the spec.
     */
    @VisibleForTesting
    @Nonnull
    static String getBasePathFrom(@Nullable final List<Server> servers) {
        if (servers == null) {
            return "/";
        }
        return servers.stream()
                .filter(Objects::nonNull)
                .map(ApiOperationResolver::substituteUrlVariables)
                .map(ApiOperationResolver::gePathFrom)
                .filter(Objects::nonNull)
                .findFirst()
                .orElse("/");
    }

    private static String gePathFrom(final String serverUrl) {
        try {
            return new URI(serverUrl).getPath();
        } catch (final URISyntaxException e) {
            log.debug("Server URL {} not a valid URI", serverUrl);
            return serverUrl;
        }
    }

    private static String substituteUrlVariables(final Server server) {
        String result = server.getUrl();
        if (result == null) {
            return "/";
        }
        if (server.getVariables() == null) {
            return result;
        }
        for (final String varName : server.getVariables().keySet()) {
            final String value = server.getVariables().get(varName).getDefault();
            result = result.replace(format("{%s}", varName), value);
        }
        return result;
    }

}
