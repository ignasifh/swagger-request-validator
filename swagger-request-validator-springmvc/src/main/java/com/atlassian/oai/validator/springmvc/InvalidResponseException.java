package com.atlassian.oai.validator.springmvc;

<<<<<<< HEAD
=======
import com.atlassian.oai.validator.report.JsonValidationReportFormat;
>>>>>>> remotes/atlassian/swagger-request-validator/master
import com.atlassian.oai.validator.report.ValidationReport;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

<<<<<<< HEAD
import java.util.stream.Collectors;

=======
>>>>>>> remotes/atlassian/swagger-request-validator/master
/**
 * In case the response is invalid.
 * <p>
 * The response will be mapped to an appropriate {@link HttpStatus}.
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class InvalidResponseException extends RuntimeException {

<<<<<<< HEAD
    private static final String MESSAGE_DELIMETER = ", ";

=======
>>>>>>> remotes/atlassian/swagger-request-validator/master
    private final ValidationReport validationReport;
    private String message;

    public InvalidResponseException(final ValidationReport validationReport) {
        this.validationReport = validationReport;
    }

    @Override
    public String getMessage() {
        if (message == null) {
<<<<<<< HEAD
            message = validationReport.getMessages().stream()
                    .map(ValidationReport.Message::getMessage)
                    .collect(Collectors.joining(MESSAGE_DELIMETER));
=======
            message = JsonValidationReportFormat.getInstance().apply(validationReport);
>>>>>>> remotes/atlassian/swagger-request-validator/master
        }
        return message;
    }

    public ValidationReport getValidationReport() {
        return validationReport;
    }
}