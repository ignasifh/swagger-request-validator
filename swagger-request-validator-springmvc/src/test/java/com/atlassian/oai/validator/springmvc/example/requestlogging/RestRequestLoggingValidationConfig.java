package com.atlassian.oai.validator.springmvc.example.requestlogging;

<<<<<<< HEAD
import com.atlassian.oai.validator.SwaggerRequestResponseValidator;
import com.atlassian.oai.validator.springmvc.ResettableRequestServletWrapper;
import com.atlassian.oai.validator.springmvc.SpringMVCLevelResolverFactory;
import com.atlassian.oai.validator.springmvc.SwaggerValidationFilter;
import com.atlassian.oai.validator.springmvc.SwaggerValidationInterceptor;
=======
import com.atlassian.oai.validator.OpenApiInteractionValidator;
import com.atlassian.oai.validator.springmvc.OpenApiValidationFilter;
import com.atlassian.oai.validator.springmvc.OpenApiValidationInterceptor;
import com.atlassian.oai.validator.springmvc.ResettableRequestServletWrapper;
import com.atlassian.oai.validator.springmvc.SpringMVCLevelResolverFactory;
>>>>>>> remotes/atlassian/swagger-request-validator/master
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.Filter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Configuration
public class RestRequestLoggingValidationConfig extends WebMvcConfigurerAdapter {

    private final OpenApiValidationInterceptor openApiValidationInterceptor;

    @Autowired
    public RestRequestLoggingValidationConfig(@Value("classpath:api-spring-test.json") final Resource swaggerSchema) throws IOException {
<<<<<<< HEAD
        final SwaggerRequestResponseValidator swaggerRequestResponseValidator = SwaggerRequestResponseValidator
=======
        final OpenApiInteractionValidator openApiInteractionValidator = OpenApiInteractionValidator
>>>>>>> remotes/atlassian/swagger-request-validator/master
                .createFor(swaggerSchema.getURL().getPath())
                .withLevelResolver(SpringMVCLevelResolverFactory.create())
                // the context path of the Spring application differs from the base path in the OpenAPI schema
                .withBasePathOverride("/v1")
                .build();
<<<<<<< HEAD
        this.swaggerValidationInterceptor = new SwaggerValidationInterceptor(swaggerRequestResponseValidator);
=======
        openApiValidationInterceptor = new OpenApiValidationInterceptor(openApiInteractionValidator);
>>>>>>> remotes/atlassian/swagger-request-validator/master
    }

    @Bean
    public Filter swaggerValidationFilter() {
<<<<<<< HEAD
        return new SwaggerValidationFilter(true, true);
=======
        return new OpenApiValidationFilter(true, true);
>>>>>>> remotes/atlassian/swagger-request-validator/master
    }

    @Override
    public void addInterceptors(final InterceptorRegistry registry) {
        // add the logging interceptor before the Swagger validation
        registry.addInterceptor(new RequestLoggingInterceptor());
        registry.addInterceptor(openApiValidationInterceptor);
    }

    private static class RequestLoggingInterceptor extends HandlerInterceptorAdapter {

        private static final Logger LOG = LoggerFactory.getLogger(RequestLoggingInterceptor.class);

        @Override
        public boolean preHandle(final HttpServletRequest servletRequest, final HttpServletResponse servletResponse,
                                 final Object handler) throws Exception {
            final String requestLog = String.join(System.lineSeparator(),
                    "uri=" + servletRequest.getRequestURI() + "?" + servletRequest.getQueryString(),
                    "client=" + servletRequest.getRemoteAddr(),
                    "payload=" + getPayload(servletRequest)
            );
            LOG.info("Incoming request: {}", requestLog);
            return true;
        }

        private String getPayload(final HttpServletRequest request) throws IOException {
            if (request instanceof ResettableRequestServletWrapper) {
                final ResettableRequestServletWrapper resettableRequest = (ResettableRequestServletWrapper) request;
                final String requestBody = IOUtils.toString(resettableRequest.getReader());
                // reset the input stream - so it can be read again by the next interceptor / filter
                resettableRequest.resetInputStream();
                return requestBody.substring(0, Math.min(1024, requestBody.length())); // only log the first 1kB
            } else {
                return "[unknown]";
            }
        }
    }
}
