package com.atlassian.oai.validator.springmvc;

<<<<<<< HEAD
import com.atlassian.oai.validator.report.ValidationReport;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
=======
import com.atlassian.oai.validator.report.JsonValidationReportFormat;
import com.atlassian.oai.validator.report.ValidationReport;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
>>>>>>> remotes/atlassian/swagger-request-validator/master

public class InvalidResponseExceptionTest {

    @Test
    public void getMessage_joinsTheValidationReportMessages() {
<<<<<<< HEAD
        final ValidationReport validationReport = Mockito.mock(ValidationReport.class);
        final ValidationReport.Message message1 = Mockito.mock(ValidationReport.Message.class);
        final ValidationReport.Message message2 = Mockito.mock(ValidationReport.Message.class);
        Mockito.when(validationReport.getMessages()).thenReturn(Arrays.asList(message1, message2));
        Mockito.when(message1.getMessage()).thenReturn("Error message 1");
        Mockito.when(message2.getMessage()).thenReturn("Error message 2");

        final InvalidResponseException classUnderTest = new InvalidResponseException(validationReport);
        Assert.assertThat(classUnderTest.getMessage(), equalTo("Error message 1, Error message 2"));
=======
        final ValidationReport validationReport = ValidationReport.from(
                ValidationReport.Message.create("dummy", "Message").build()
        );

        final InvalidResponseException classUnderTest = new InvalidResponseException(validationReport);
        assertThat(classUnderTest.getMessage(),
                equalTo(JsonValidationReportFormat.getInstance().apply(validationReport)));
>>>>>>> remotes/atlassian/swagger-request-validator/master
    }

    @Test
    public void getMessage_isEmptyInCaseOfNoErrors() {
<<<<<<< HEAD
        final ValidationReport validationReport = Mockito.mock(ValidationReport.class);
        Mockito.when(validationReport.getMessages()).thenReturn(Collections.emptyList());

        final InvalidResponseException classUnderTest = new InvalidResponseException(validationReport);
        Assert.assertThat(classUnderTest.getMessage(), equalTo(""));
=======
        final ValidationReport validationReport = ValidationReport.from();

        final InvalidResponseException classUnderTest = new InvalidResponseException(validationReport);
        assertThat(classUnderTest.getMessage(), equalTo("{ }"));
>>>>>>> remotes/atlassian/swagger-request-validator/master
    }

    @Test
    public void getValidationReport() {
<<<<<<< HEAD
        final ValidationReport validationReport = Mockito.mock(ValidationReport.class);

        final InvalidResponseException classUnderTest = new InvalidResponseException(validationReport);
        Assert.assertThat(classUnderTest.getValidationReport(), is(validationReport));
=======
        final ValidationReport validationReport = ValidationReport.from(
                ValidationReport.Message.create("dummy", "Message").build()
        );

        final InvalidResponseException classUnderTest = new InvalidResponseException(validationReport);
        assertThat(classUnderTest.getValidationReport(), is(validationReport));
>>>>>>> remotes/atlassian/swagger-request-validator/master
    }
}
